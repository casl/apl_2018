/****************************
Filename : Jumping Kangaroo with M coins.cpp
Assignment : APL HW-1
Author : Ajay Sharma (CS17M008)
Date : 30 July 2018
Description : This file implements problem "Jumping Kangaroo with M coins" using iterative approach.
****************************/

#include<iostream>
#include<stdlib.h>

using namespace std;

/****************
	Function to calculate minimum of two values given.
****************/
int min(int n,int max)
{
	if(n>max)
	{
		return max;
	}
	else
	{
		return n;
	}
}

/****************
	Iterative function to compute number of ways for given value of n given m coins.
	LOGIC: Compute for each coin spent which stairs you can reach and in how many ways.
****************/
long long computeWays(int n,int m)
{
	long long *steps = (long long*) calloc(n+1,sizeof(long long));
	int max=0;
	steps[0]=1;
	for(int i=0;i<m;i++)
	{
		for(int j=min(n,max);j>=i;j--)
		{
			if(j+1<=n)
			{
				steps[j+1]+=steps[j];
			}
			if(j+2<=n)
			{
				steps[j+2]+=steps[j];
			}
			if(j+3<=n)
			{
				steps[j+3]+=steps[j];
			}
			
			steps[j]=0;
		}
		
		max+=3;
	}
	
	return steps[n];
}

int main()
{
	int q;
	cin>>q;
	
	while(q--)
	{
		int n,m;
		cin>>n>>m;
		
		long long no_of_ways = computeWays(n,m);
	
		cout<<no_of_ways<<endl;	
	}
		
	return 0;
}
