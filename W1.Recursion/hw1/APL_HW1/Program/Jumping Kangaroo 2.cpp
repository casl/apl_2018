/****************************
Filename : Jumping Kangaroo 2.cpp
Assignment : APL HW-1
Author : Ajay Sharma (CS17M008)
Date : 30 July 2018
Description : This file implements problem "Jumping Kangaroo 2" using iterative approach with O(n) complexity and constant space.
****************************/

#include<iostream>

using namespace std;

/****************
	Iterative function to compute number of ways for given value of n.
	LOGIC: No of ways to reach a particular step is equal to sum of no of ways to reach it's previous three steps.
****************/
long long computeWays(int n)
{
	long long first=1,second=2,third=4;
	
	if(n==1)
	{
		return first;
	}
	else if(n==2)
	{
		return second;
	}
	else if(n==3)
	{
		return third;
	}
	
	long long no_of_ways;
	
	for(int stair=4;stair<=n;stair++)
	{
		no_of_ways = first + second + third;
		
		first = second;
		second = third;
		third = no_of_ways;
	}
	
	return no_of_ways;
}

int main()
{
	int q;
	cin>>q;
	
	while(q--)
	{
		int n;
		cin>>n;
	
		long long no_of_ways = computeWays(n);
	
		cout<<no_of_ways<<endl;
	}
	
	return 0;
}
