/****************************
Filename : Jumping Kangaroo 1.cpp
Assignment : APL HW-1
Author : Ajay Sharma (CS17M008)
Date : 30 July 2018
Description : This file implements problem "Jumping Kangaroo 1" using recursive approach.
****************************/

#include<iostream>

using namespace std;

/****************
	Recursive function to compute number of ways for given value of n.
	Here present_step represent the current step you are processing in stairway.
****************/
long long computeWays(int n,long long present_step)
{
	if(present_step > n)
	{
		return 0;
	}
	else if(present_step == n)
	{
		return 1;
	}
	
	long long no_of_ways = computeWays(n,present_step+1)+computeWays(n,present_step+2)+computeWays(n,present_step+3);
	
	return no_of_ways;
}

int main()
{
	int n;
	cin>>n;
	
	long long no_of_ways = computeWays(n,0);
	
	cout<<no_of_ways<<endl;
	
	return 0;
}
