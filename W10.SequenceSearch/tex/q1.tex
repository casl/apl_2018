\section*{Repeated Subsequence Search}
Let $\mathbb A$ be a set of alphabets. For example, 
$$
\mathbb A = \{a, b, c, d\}\enspace.
$$
Define two strings in $\mathbb A$. We will call them $S$ and $T$
with string lengths $n$ and $m$ respectively.
For example let $S = abdabcadabccdabdccdaabcd$ and $T=ab$.
Define $T_i$ ($i \ge 0$) to be $T$ with each alphabet repeated $i$ times.
For example,
$T_0$ is $\phi$,
$T_1$ is $ab$,
$T_2$ is $aabb$, 
$T_3$ is $aaabb$, 
$T_4$ is $aaaabbbb$ and so on.

The problem we deal with here is to find the largest value of $i$ such
that $T_i$ forms a subsequence of $S$. Note that $T_i$ does not have to
be a substring of $S$. The table below shows how we can exhaustively search
for $i$ in $S$.
\begin{table}[!h]
\begin{center}
\begin{tabular}{cll}
	\hline
	$i=1$   & ${\color{red} a}{\color{red} b}dabcadabccdabdccdaabcd$ & Subsequence found \\
	$i=2$   & ${\color{red} a}bd{\color{red} a}{\color{red} b}cada{\color{red} b}ccdabdccdaabcd$ & Subsequence found \\
	$i=3$   & ${\color{red} a}bd{\color{red} a}bc{\color{red} a}da{\color{red} b}ccda{\color{red} b}dccdaa{\color{red} b}cd$ & Subsequence found \\
	$i=4$   & ${\color{red} a}bd{\color{red} a}bc{\color{red} a}d{\color{red} a}{\color{red} b}ccda{\color{red} b}dccdaa{\color{red} b}cd$ & Subsequence not found \\
	\hline
\end{tabular}
\end{center}
\end{table}

From the above table, it is evident that the largest value of $i$ is 3.
We denote this as $i_{max}$.

The obvious way to find $i_{max}$ is by brute force, where we start with $i = 1$
and then increment $i$ until we hit $i_{max}$. Since $i_{max} \le n/m$, and the
time taken to make one subsequence search is $O(n)$ (assuming $n \ge m$), thus,
the exhaustive search has a complexity $O(n^2/m)$.

The objective of this assignment is to provide a divide-and-conquer algorithm,
so that the complexity of the program reduces to $O(n \log n)$.

{\flushleft \bf Hint.}
Your first thought would be to split $S$ into two halves, as is 
typically done in a divide-and-conquer approach. However, this will not
work out, beause we want to search for substrings which can extend across the entire $S$.
You would need a better splitting strategy!


\section*{For the TAs}
A better approach is to 
split $S$ into two strings $S_1$ and $S_2$ such that every even occurance
of an alphabet in $S$ goes to $S_2$, while every odd occurance of the
alphabet goes to $S_1$. Thus, we can split $S$ as follows:
\begin{verbatim}
	S   = abdabcadabccdabdccdaabcd
	S_1 = abd  ca  b cda   cd ab     
	S_2 =    ab  da c   bdc  a  cd  
\end{verbatim}

Let $i_{max_1}$ and $i_{max_2}$ be the max subsequence length for $S_1$ and $S_2$ respectively.
If $i_{max}$ is even, then $i_{max} = i_{max_1} + i_{max_2}$ and $i_{max_1} = i_{max_2}$.
For example, consider {\tt T = ab}.
\begin{verbatim}
	S   = abcdabcdabcdabcd       i_max = 2
	S_1 = abcd    abcd           i_max1 = 1
	S_2 =     abcd    abcd       i_max2 = 1
\end{verbatim}

If $i_{max}$ is odd, then $i_{max} = i_{max_1} + i_{max_2} \pm 1$.
For example, consider {\tt T = ab}
\begin{verbatim}
	S   = bacdbacdaacd            i_max  = 1
	S_1 = bacd    aacd            i_max1 = 0
	S_2 =     bacd                i_max2 = 0
	                              
\end{verbatim}
Similarly, 
\begin{verbatim}
	S   = abcdabcdabcd            i_max  = 1 
	S_1 = abcd    abcd            i_max1 = 1
	S_2 =     abcd                i_max2 = 1
\end{verbatim}

Thus, $i_{max}$ is one of these 3:$\{i_{max_1} + i_{max_2} - 1, i_{max_1} + i_{max_2}, i_{max_1} + i_{max_2} + 1\}$.
During the conquer part,
we check each of these 3 possibilities using the exhaustive approach to find out which works.



