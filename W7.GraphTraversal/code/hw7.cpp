#include<bits/stdc++.h>
#include<fstream>

using namespace std;

int main()
{
	int n,e;
	cin>>n>>e;
	
	vector<vector<int> > graph(n);
	
	for(int i=0;i<e;i++)
	{
		int start,end;
		cin>>start>>end;
		
		graph[start].push_back(end);
	}
	
	set<int> result;
	set<int> visited;
	
	for(int i=0;i<n;i++)
	{
		visited.insert(i);
	}
	
	while(!visited.empty())
	{
		int n_visited = *(visited.begin());
		visited.erase(n_visited);
			
		deque<int> work;
		work.push_back(n_visited);
		
		while(!work.empty())
		{
			int index = work.front();
			work.pop_front();
			
			for(int i=0;i<graph[index].size();i++)
			{
				if(visited.find(graph[index][i])!=visited.end())
				{
					visited.erase(graph[index][i]);
					work.push_back(graph[index][i]);	
				}
				
				if(result.find(graph[index][i])!=result.end())
				{
					result.erase(graph[index][i]);	
				}	
			}	
		}	
		
		result.insert(n_visited);
	}
	
	for(set<int>::iterator itr=result.begin();itr!=result.end();itr++)
	{
		cout<<*itr<<" ";
	}
	
	cout<<endl;
	
	int max_hop=0;
	int level_no = -1;
	deque<int> work_hop;
	vector<int> reached(n,0);
	for(set<int>::iterator itr=result.begin();itr!=result.end();itr++)
	{
		work_hop.push_back(*itr);
		reached[*itr]=1;	
	}
	work_hop.push_back(level_no);
		
	while(!work_hop.empty())
	{
		int index = work_hop.front();
		
		work_hop.pop_front();
			
		if(index < 0)
		{
			work_hop.push_back(--level_no);
			if(work_hop.front() < 0)
			{
				work_hop.pop_front();
				break;
			}
		}
		else
		{
			for(int i=0;i<graph[index].size();i++)
			{
				if(reached[graph[index][i]]==0)
				{
					reached[graph[index][i]]=1;
					work_hop.push_back(graph[index][i]);	
				}	
			}	
		}	
	}
	
	level_no*=(-1);
	level_no--;
	
	if(max_hop < level_no)
	{
		max_hop = level_no;	
	}	
	
	cout<<max_hop<<endl;
	
		
	return 0;
}
