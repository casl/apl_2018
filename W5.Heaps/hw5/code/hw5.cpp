//Himanshu gaur
#include<stdio.h>
#include<bits/stdc++.h>
using namespace std;
#define MAXXX pow(10,6);

class point
{
	public:
		int l;
		int r;
		int dist;
};

//function for heapify
void adjust(point **p , int s, int e)
{
	int j,item, iteml,itemr;
	j = 2*s;
	item = p[s]->dist;
	iteml = p[s]->l;
	itemr = p[s]->r;
	while(j<= e)
	{
		if(j<e && p[j]->dist > p[j+1]->dist)
			j++;
		if(p[j]->dist >= item)
			break;
		p[j/2]->dist = p[j]->dist;
		p[j/2]->l = p[j]->l;
		p[j/2]->r = p[j]->r;

		j = j*2;
	}

	p[j/2]->dist = item;
	p[j/2]->l = iteml;
	p[j/2]->r = itemr;
}
//delete minimum
point* deletemin(point **p, int n)
{
	point *temp_p = new point();
	temp_p->l = p[1]->l;
	temp_p->r = p[1]->r;
	temp_p->dist = p[1]->dist;

	p[1]->l = p[n]->l;
	p[1]->r = p[n]->r;
	p[1]->dist = p[n]->dist;

	adjust(p,1,n-1);

	return temp_p;
}
//insert element in heap
void insert(point **p,int n,point *add)
{
	int i = n;
	while(i>1 && p[i/2]->dist > add->dist)
	{
		p[i]->l = p[i/2]->l;
		p[i]->r = p[i/2]->r;
		p[i]->dist = p[i/2]->dist;

		i = i/2;
	}
	p[i]->l = add->l;
	p[i]->r = add->r;
	p[i]->dist = add->dist;

}

int main(int argc, char *argv[])
{
	//Input 
	FILE *fp;	
	int n_p,heap_size,k,cost;
	int **d_mat , *visited;

	//cin>>n_p;
	fp = fopen(argv[1],"r");
	fscanf(fp,"%d%d",&n_p,&k);

	d_mat = new int*[n_p];
	heap_size = n_p -1;	
	visited = new int[n_p+1];
	point **p = new point*[n_p];


	visited[0] = 0;
	visited[n_p] = 0;

	point *hg = new point();
	hg->l = 0;
	hg->r = 0;
	hg->dist = MAXXX;

	p[0] = hg;	
	for(int i=1;i<n_p;i++)
	{
		d_mat[i] = new int[3];
		visited[i] = 0;
		point *temp = new point();
		fscanf(fp,"%d%d%d",&temp->l,&temp->r,&temp->dist);
		d_mat[i][0] = temp->l;
		d_mat[i][1] = temp->r;
		d_mat[i][2] = temp->dist;
		p[i]=temp;
	}

	//build heap O(n)
	for(int i = n_p/2 ; i>= 1 ;i--)
	{
		adjust(p,i,n_p-1);
	}

	cost = 0;
	//loop for k values
	for(int i = k ; i >0 ;)
	{
		point *tt = deletemin(p,heap_size);
		heap_size--;

		if(!visited[tt->l] && !visited[tt->r] ) //if element not visited the add in cost
		{
			visited[tt->l] = 1;
			visited[tt->r] = 1;
			cost = cost + tt->dist;
			int val=0;
			if((tt->l)-1 >= 1 && (tt->r)+1 <= n_p) //checking for validity of new point
			{
				val = d_mat[(tt->l)-1][2] + d_mat[(tt->r)][2] -  tt->dist;  //virtual cost calculation
				point *temp_pt = new point();
				temp_pt->l = tt->l-1;
				temp_pt->r = tt->r+1;
				temp_pt->dist = val;
				insert(p,heap_size+1, temp_pt); //insert virtual cost in heap
				heap_size++;				
			}
			i--;
		}
	}
	cout<<cost<<endl;;

	return 0;
}





















// for(int i=0 ; i<n_p-1 ; i++)
	// 	cout<<p[i]->l<<" "<<p[i]->r<<" "<<p[i]->dist<<endl;
