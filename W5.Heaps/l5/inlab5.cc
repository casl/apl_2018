// C++ program for implementation of Heap Sort
#include <iostream>
using namespace std;


struct pq
{
	int key;
	int val_i, val_j;
};

int parent(int i) { return (i-1)/2; }

void heapify_pq(struct pq arr[], int n, int i)
{
	int smallest = i; // Initialize smallest as root
	int l = 2*i + 1; // left = 2*i + 1
	int r = 2*i + 2; // right = 2*i + 2

	// If left child is smaller than root
	if (l < n && arr[l].key < arr[smallest].key)
		smallest = l;

	// If right child is smaller than smallest so far
	if (r < n && arr[r].key < arr[smallest].key)
		smallest = r;

	// If smallest is not root
	if (smallest != i)
	{
		swap(arr[i], arr[smallest]);

		// Recursively heapify the affected sub-tree
		heapify_pq(arr, n, smallest);
	}
}


void insertKey(struct pq arr[], int i, int j, int k, int *sz)
{
    // if (*sz == capacity)
    // {
    //     // cout << "\nOverflow: Could not insertKey\n";
    //     return;
    // }
 
    // First insert the new key at the end
    for(int x=0; x<(*sz);x++)
    {
    	if(arr[x].val_i==i && arr[x].val_j==j)
    		return;
    }
    
    (*sz)++;
    int s = (*sz) - 1;
    arr[s].val_i=i;
    arr[s].val_j=j;
    arr[s].key=k;

    	
    // Fix the min heap property if it is violated
    while (s != 0 && arr[parent(s)].key > arr[s].key)
    {
       swap(arr[s], arr[parent(s)]);
       s = parent(s);
    }
}

struct pq extractMin(struct pq arr[], int *sz)
{
	if (*sz == 1)
    {
        (*sz)--;
        return arr[0];
    }
 
    struct pq root = arr[0];
    arr[0] = arr[(*sz)-1];
    (*sz)--;
    heapify_pq(arr, *sz, 0);
 
    return root;
}




void heapify(int arr[], int n, int i)
{
	int smallest = i; // Initialize smallest as root
	int l = 2*i + 1; // left = 2*i + 1
	int r = 2*i + 2; // right = 2*i + 2

	if (l < n && arr[l] < arr[smallest])
		smallest = l;

	
	if (r < n && arr[r] < arr[smallest])
		smallest = r;

	
	if (smallest != i)
	{
		swap(arr[i], arr[smallest]);
		heapify(arr, n, smallest);
	}
}


void buildHeap(int arr[], int n)
{
	// Build heap (rearrange array)
	for (int i = n / 2 - 1; i >= 0; i--)
		heapify(arr, n, i);

}

/* A utility function to print array of size n */
void printArray(int arr[], int n)
{
	for (int i=0; i<n; i++)
		cout << arr[i] << " ";
	cout << "\n";
}

// Driver program
int main()
{
	
	int n, m, capacity;

	cin>>n>>m>>capacity;
	// cout<<m<<" "<<n<<" "<<capacity<<"\n";
	if(capacity > m*n)
	{ 
		cout<<"Invalid Input";
		return 0;
	}

	int A[n], B[m];

	for(int i=0;i<n;i++)
		cin>>A[i];

	for(int i=0;i<m;i++)
		cin>>B[i];
	
	buildHeap(A, n);
	// printArray(A, n);
	
	buildHeap(B, m);
	// printArray(B, m);

	int sz=0;
	struct pq arr[capacity*capacity];
	insertKey(arr, 0, 0, A[0]+B[0], &sz);


	for(int x=0; x<capacity; x++)
	{
		struct pq xmin= extractMin(arr, &sz);
		cout << xmin.key << " " ;
		
		if((2*xmin.val_i+1) < n )
			insertKey(arr, (2*xmin.val_i)+1, (xmin.val_j), A[(2*xmin.val_i + 1)]+B[(xmin.val_j)], &sz);
		
		if((2*xmin.val_i+2) < n )
			insertKey(arr, (2*xmin.val_i)+2, (xmin.val_j), A[(2*xmin.val_i)+2]+B[(xmin.val_j)], &sz);
		
		if((2*xmin.val_j+1) < m )
			insertKey(arr, (xmin.val_i), (2*xmin.val_j +1), A[(xmin.val_i)]+B[(2*xmin.val_j)+1], &sz);
		
		if((2*xmin.val_j+2) < m )
			insertKey(arr, (xmin.val_i), (2*xmin.val_j)+2, A[(xmin.val_i)]+B[(2*xmin.val_j)+2], &sz);
		
	}
	cout<<endl;



}
