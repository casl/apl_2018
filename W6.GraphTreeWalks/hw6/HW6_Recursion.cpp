#include<iostream>
#include<stdio.h>
#include<string.h>
#include<string>
#include<map>
#include<algorithm>
#include<vector>
#include<queue>
#define ll long long int
using namespace std;
const int inf=2000000000;
int a[1024][1024],dc[1024][1024],dd[1024][1024];
queue<pair<int,int> > q;
pair<int,int> pr;
void update_dc(int x,int y,int dis)
{
    pair<int,int> tmp;
    if(dis<dc[x][y])
    {
        dc[x][y]=dis;
        tmp.first=x;
        tmp.second=y;
        q.push(tmp);
    }
}
void update_dd(int x,int y,int dis)
{
    pair<int,int> tmp;
    if(dis<dd[x][y])
    {
        dd[x][y]=dis;
        tmp.first=x;
        tmp.second=y;
        q.push(tmp);
    }
}
int main()
{
    int i,j,t,n,m,x,y,v,dx,dy,dis;
    scanf("%d",&t);
    while(t--)
    {
        scanf("%d %d",&n,&m);
        for(i=0;i<n;i++)
        {
            for(j=0;j<m;j++)
            {
                scanf("%d",&v);
                if(v==0)
                    a[i][j]=2;
                else if(v==1)
                    a[i][j]=0;
                else
                    a[i][j]=1;

                dc[i][j]=inf;
                dd[i][j]=inf;
            }
        }
        scanf("%d %d",&dx,&dy);
    if(a[0][0]==2)
    {
        printf("-1\n");
        continue;
    }

    dc[0][0]=0;
    pr.first=0;
    pr.second=0;
    q.push(pr);
    while(!q.empty())
    {
        pr=q.front();
        x=pr.first;
        y=pr.second;
        q.pop();
        if(dc[x][y]!=inf)
        {
            dis=dc[x][y];

            if(a[x][y]==1)
            {
                if(x+3<n)
                {
                    if(a[x+1][y]!=2 && a[x+2][y]!=2 && a[x+3][y]!=2)
                        update_dd(x+3,y,dis+1);
                }
                if(x-3>=0)
                {
                    if(a[x-1][y]!=2 && a[x-2][y]!=2 && a[x-3][y]!=2)
                        update_dd(x-3,y,dis+1);
                }
                if(y+3<m)
                {
                    if(a[x][y+1]!=2 && a[x][y+2]!=2 && a[x][y+3]!=2)
                        update_dd(x,y+3,dis+1);
                }
                if(y-3>=0)
                {
                    if(a[x][y-1]!=2 && a[x][y-2]!=2 && a[x][y-3]!=2)
                        update_dd(x,y-3,dis+1);
                }
            }
            else
            {
                if(x+2<n)
                {
                    if(a[x+1][y]!=2 && a[x+2][y]!=2)
                        update_dc(x+2,y,dis+1);
                }
                if(x-2>=0)
                {
                    if(a[x-1][y]!=2&&a[x-2][y]!=2)
                        update_dc(x-2,y,dis+1);
                }
                if(y+2<m)
                {
                    if(a[x][y+1]!=2&&a[x][y+2]!=2)
                        update_dc(x,y+2,dis+1);
                }
                if(y-2>=0)
                {
                    if(a[x][y-1]!=2&&a[x][y-2]!=2)
                        update_dc(x,y-2,dis+1);
                }
            }
        }


        if(dd[x][y]!=inf)
        {
            dis=dd[x][y];

            if(a[x][y]==0)
            {
                if(x+3<n)
                {
                    if(a[x+1][y]!=2 && a[x+2][y]!=2 && a[x+3][y]!=2)
                        update_dd(x+3,y,dis+1);
                }
                if(x-3>=0)
                {
                    if(a[x-1][y]!=2 && a[x-2][y]!=2 && a[x-3][y]!=2)
                        update_dd(x-3,y,dis+1);
                }
                if(y+3<m)
                {
                    if(a[x][y+1]!=2 && a[x][y+2]!=2 && a[x][y+3]!=2)
                        update_dd(x,y+3,dis+1);
                }
                if(y-3>=0)
                {
                    if(a[x][y-1]!=2 && a[x][y-2]!=2 && a[x][y-3]!=2)
                        update_dd(x,y-3,dis+1);
                }
            }
            else
            {
                if(x+2<n)
                {
                    if(a[x+1][y]!=2 && a[x+2][y]!=2)
                        update_dc(x+2,y,dis+1);
                }
                if(x-2>=0)
                {
                    if(a[x-1][y]!=2&&a[x-2][y]!=2)
                        update_dc(x-2,y,dis+1);
                }
                if(y+2<m)
                {
                    if(a[x][y+1]!=2&&a[x][y+2]!=2)
                        update_dc(x,y+2,dis+1);
                }
                if(y-2>=0)
                {
                    if(a[x][y-1]!=2&&a[x][y-2]!=2)
                        update_dc(x,y-2,dis+1);
                }
            }
        }
    }


    int ans;

    if(dc[dx][dy]<dd[dx][dy])
        ans=dc[dx][dy];
    else
        ans=dd[dx][dy];
    if(ans==inf)
        printf("-1\n");
    else
        printf("%d\n",ans);
    }
    return 0;
}
