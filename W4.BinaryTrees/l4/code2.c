#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>

#define NUM_MAX_SIZE  64

struct node
{
    struct node *left;
    struct node *right;
    int color;
};

struct data_node
{
    struct data_node* link;
    int value;
};

int height(struct node* node);
void insert_num(int num);
void delete_num(int num);
void print_num();
void printGivenLevel(struct node* ptr, int level, int num);
int convert2Decimal(int num);


struct node * root;
int data_arr_count = 0;
struct data_node *data_arr,*data_ptr;
int total_numbers_in_tree = 0;

void insert_num(int num)
{
    int bit_arr[NUM_MAX_SIZE];
    int count=0;
    while(num!=0)
    {
        bit_arr[count] = num%2;
        num = num/2;
        count++;        
    }
    if(root == '\0')
    {
        root = (struct node*)malloc(sizeof(struct node));
        root->color = 0;
        root->left = '\0';
        root->right = '\0';
    }
    struct node *ptr = root;
    if(num != 1)
    {        
        for(int i =count-2;i>=0;i--)
        {
            if(bit_arr[i] == 1)
            {
                if(ptr->right == '\0')
                {
                    struct node *new_ptr = (struct node*)malloc(sizeof(struct node));
                    new_ptr->color = 0;
                    new_ptr->left = '\0';
                    new_ptr->right = '\0';
                    ptr->right = new_ptr;
                    ptr = new_ptr;
                }
                else
                {
                    ptr = ptr->right;    
                }
            }
            else
            {
                if(ptr->left == '\0')
                {
                    struct node *new_ptr = (struct node*)malloc(sizeof(struct node));
                    new_ptr->color = 0;
                    new_ptr->left = '\0';
                    new_ptr->right = '\0';
                    ptr->left = new_ptr;
                    ptr = new_ptr;
                }
                else
                {
                    ptr = ptr->left;    
                }
            }
        }
    }
    ptr->color = 1;
    total_numbers_in_tree++;
}

void delete_num(int num)
{
    int bit_arr[NUM_MAX_SIZE];
    int count=0;
    while(num!=0)
    {
        bit_arr[count] = num%2;
        num = num/2;
        count++;        
    }
    struct node *ptr = root;
    struct node *parent=root;
    int left_right = -1; // 1 for right and 0 for left
    for(int i = count-2;i>=0;i--)
    {
        if(ptr == '\0')
            break;
        if(bit_arr[i] == 1)
        {
            if(ptr->color == 1 || ptr->left != '\0')
            {
                parent = ptr;
                left_right = 1;
            }
            ptr = ptr->right;
        }
        else
        {
            if(ptr->color == 1 || ptr->right != '\0')
            {
                parent = ptr;
                left_right = 0;
            }
            ptr = ptr->left;                        
        }
    }

    if(ptr == '\0' || ptr->color == 0 || total_numbers_in_tree == 0)
    {
        // printf("\n");
        if((ptr == root && root == '\0') || total_numbers_in_tree==0)
        {
            printf("String is not present\n");                        
        }
        else
        {
            if(ptr == '\0')
            {
                printf("String is not present\n");
            }
            else if(ptr->color == 0)
            {
                printf("Node is white\n");
            }
            // printf("Number to Delete is not Present\n");
        }
    }
    else if(ptr->left == '\0' && ptr->right == '\0')
    {
        if(ptr == root)
        {
            root = '\0';
        }
        else if(left_right == 1)
        {
            parent->right = '\0';
        }
        else
        {
            parent->left = '\0';
        }
        total_numbers_in_tree--;
    }
    else if(ptr != '\0')
    {        
        ptr->color = 0;
        total_numbers_in_tree--;
    }

    if(total_numbers_in_tree == 0)
    {
        root = '\0';
    }
    
}

int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}


void print_num()
{
    int h = height(root);
    int i;
    // data_arr = (int*)malloc(((2^height(root))-1)*sizeof(int));
    data_arr = '\0';
    data_arr_count = 0;
    for (i=1; i<=h; i++)
        printGivenLevel(root, i,1);

      // printf("\n");
      data_ptr = data_arr;
      int sorted_arr[data_arr_count];
    for(int i = 0;i<data_arr_count;i++)
    {
        // printf("%d ",data_ptr->value);
        sorted_arr[i] = data_ptr->value;
        data_ptr = data_ptr->link;
    }

    qsort(sorted_arr,data_arr_count,sizeof(int),cmpfunc);

    if(data_arr != '\0')
    {
            for(int i = 0;i<data_arr_count;i++)
            {
                printf("%d ",sorted_arr[i]);
            }            
        printf("\n");
    }
    else
    {
        printf("Empty Tree\n");
    }

    // printf("\n");
}

/* Print nodes at a given level */
void printGivenLevel(struct node* ptr, int level, int num)
{
    if (ptr == NULL)
        return;
    if (level == 1 && ptr->color == 1)
    {
        struct data_node* new_val = (struct data_node*)malloc(sizeof(struct data_node));
        new_val->link = '\0';
        new_val->value = convert2Decimal(num);
        if(data_arr == '\0')
        {
            data_arr = new_val;
            data_ptr = data_arr;
        }
        else
        {
            data_ptr->link = new_val;
            data_ptr = new_val;
        }
        // data_arr[data_arr_count] = ;
        data_arr_count++;    
    }

    else if (level > 1)
    {
        printGivenLevel(ptr->left, level-1,(10*num + 0));
        printGivenLevel(ptr->right, level-1,(10*num + 1));
    }
}

int height(struct node* node)
{
    if (node==NULL)
        return 0;
    else
    {
        /* compute the height of each subtree */
        int lheight = height(node->left);
        int rheight = height(node->right);
 
        /* use the larger one */
        if (lheight > rheight)
            return(lheight+1);
        else return(rheight+1);
    }
}
 
int convert2Decimal(int num)
{
    int new_num = 0;
    int i = 1;
    while(num != 0)
    {
        int rem = num%10;
        new_num = new_num + rem*(i);
        num = num/10;
        i*=2;
    }

    return  new_num;
}

int main()
{
    int choice;
    // printf("Choices:- \n");
    // printf("1.Insert a Number\n");
    // printf("2.Delete a Number\n");
    // printf("3.Print the Numbers in the Tree\n");
    // printf("4.Exit\n");
    int flag = 1;
    while(flag)
    {
        // printf("Choices:- \n");
        // printf("1.Insert a Number\n");
        // printf("2.Delete a Number\n");
        // printf("3.Print the Numbers in the Tree\n");
        // printf("4.Exit\n");        
        scanf("%d",&choice);
        int num;        
        switch(choice)
        {            
            case 1:
                // printf("Enter the number to Insert");
                scanf("%d",&num);
                // printf("\nNumber to insert is %d\n", num);
                insert_num(num);
                break;
            case 2:
                // printf("Enter the number to Delete");
                scanf("%d",&num);
                // printf("\nNumber to delete is %d\n", num);                
                delete_num(num);                
                break;
            case 3:
                print_num();
                break;                
            case 4:
                flag = 0;
                break;
        }
    }
    return 0;
}



