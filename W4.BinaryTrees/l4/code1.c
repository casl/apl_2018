#include <iostream>
#include <bits/stdc++.h>

using namespace std;

struct Node{
    int val;
    char color;
    Node* left;
    Node* right;
};

Node* newNode(int val,char c)
{
    Node* temp=new Node;
    temp->val=val;
    temp->color=c;
    temp->left=temp->right=NULL;
    return temp;
}

string toBinary(int num)
{
    string s;
    int bin;
    while (num > 0)
    {
        bin = num % 2;
        s.append(to_string(bin));
        num /= 2;
    }
    std::reverse(s.begin(),s.end());
    return s;
}
void insertNode(Node* node, string s, int index)
{

    if(index>=s.size())
	return;
    if(index==s.size()-1)
    {
        if(s[index]=='0')
        {
        	if(node->left==NULL)
        		node->left=newNode(node->val*2,'b');
        	else
        		node->left->color='b';
        }
            
        else
            if(s[index]=='1')
            {
            	if(node->right==NULL)
        		node->right=newNode(node->val*2+1,'b');
        		else
        		node->right->color='b';
            	
            }
    }
    else
    {
        if(s[index]=='0')
            {
            	if(node->left==NULL)
            		node->left=newNode(node->val*2,'w');
            insertNode(node->left,s,index+1);}
        else
            if(s[index]=='1')
                {
                	if(node->right==NULL)
                	node->right=newNode(node->val*2+1,'w');
                insertNode(node->right,s,index+1);}
        
    }
}

void traverse(Node* root)
{
    queue<Node*> q;
	if(!root)
	cout<<"Empty Tree";
    if (root) {
        q.push(root);
    }
    while (!q.empty())
    {
        Node* temp_node = q.front();
        q.pop();
        if(temp_node->color=='b')
	cout<<temp_node->val<<" ";
        if (temp_node->left) {
            q.push(temp_node->left);
        }
        if (temp_node->right) {
            q.push(temp_node->right);
        }
    }
}


bool findChild(Node* temp)
{
	if(temp==NULL)
		return false;
	if(temp->color=='b')
		return true;
	if(temp->left==NULL && temp->right==NULL)
		return false;
	if(temp->left && temp->left->color=='b')
		return true;
	if(temp->right && temp->right->color=='b')
		return true;
	return (findChild(temp->left) || findChild(temp->right));
}

Node* deleteNode(Node* node,string s)
{
	Node*temp=node;
	stack<Node*> st;
	if(temp==NULL)
		{cout<<"String is not present"<<endl;
		return node;}
	st.push(temp);
	for(int i=1;i<s.size();i++)
	{
		if(s[i]=='0')
		{
			if(temp->left==NULL)
				{cout<<"String is not present"<<endl;
				return node;}
			st.push(temp->left);
			temp=temp->left;
		}
		else
		if(s[i]=='1')
		{
			if(temp->right==NULL)
				{cout<<"String is not present"<<endl;
				return node;}
			st.push(temp->right);
			temp=temp->right;
		}
	}
	if(temp->color=='w')
	{
		cout<<"Node is white"<<endl;
		return node;
	}
	temp->color='w';
	while(!findChild(temp))
	{
		Node* tofree=st.top();
		st.pop();
		if(!st.empty())
		{
			temp=st.top();
			if(temp->left==tofree)
				temp->left=NULL;
			else
			if(temp->right==tofree)
				temp->right=NULL;
			free(tofree);	
		}
		else 
		{
			node=NULL;
			break;
		}
		
	}
	return node;
}

int main() {
	int q,value;
    cin>>q;
    Node* root=NULL; 
    while(q!=4)
    {
        if(q==1)
        {
            int value;
            cin>>value;
            string s=toBinary(value);
            if(value && root==NULL)
            	root=newNode(1,'w');
            if(value==1)
            	root->color='b';
            insertNode(root, s, 1);
        }
        else
        if(q==3)
        {
        	traverse(root);
        	cout<<endl;
        }
        else
        if(q==2)
        {
            cin>>value;
            string s=toBinary(value);
	root=deleteNode(root, s);
        }
        cin>>q;
    }
	return 0;
}
