from matplotlib.pyplot import *
from random import *

## Display the map in a python plot
def showmap():
	fd = open("image.txt", "r")

	while(1):
		line = fd.readline()
		if (line == ''): break
		[x, y, rc, bc, gc] = line.split()
		x = int(x); y=int(y); rc=int(rc)/256.0; bc=int(bc)/256.0; gc=int(gc)/256.0
		c = [rc, gc, bc]
		scatter(x, y, color=c)

	show()
	fd.close()

## Compare points. Returns true if x and y coordinates of
## pt1 is > x and y coordinates of pt2
def compare_points(pt1, pt2):
	if (pt1[0] >= pt2[0] and pt1[1] >= pt2[1]): 
		return 1
	else:
		return 0

## Define the countours here
## Each contour is a 6 tuple defined as follows:
## [x1, y1, x2, y2, r, g, b]
## The contour is defined as a rectangle from (x1, y1) to (x2, y2)
## and represented with the color (r, g, b)
CONTOURS = [[10, 5, 11, 9, 255, 0, 0], 
            [11, 6, 14, 17, 0, 255, 0],
            [12, 8, 14, 20, 255, 255, 0] ]

## Checks if the point x, y is in any one of defined CONTOURS
## If (x, y) is in exactly one contour, then return the corresponding (r, g, b) color.
## If (x, y) is present in multiple contours, then return the average color formed
## by overlapping all individual colors
def check_contour(x, y):
	r = 0; g = 0; b = 0; 
	count = 0
	for c in CONTOURS:
		if (compare_points((x, y), (c[0], c[1])) and compare_points((c[2], c[3]), (x, y))):
			r += c[4]; g += c[5]; b += c[6]; count += 1
			print x, y, c, count
	if (count != 0):
		return r/count, b/count, g/count
	else:
		return 0, 255, 0 

## Create the image
def makemap1():
	fd = open("image.txt", "w")
	for x in range(20):
		for y in range(20):
			r, g, b = check_contour(x, y)
			buf = str(x) + ' ' + str(y) + ' ' + str(r) + ' '  + str(g) + ' ' + str(b) + '\n'
			fd.write(buf)
	fd.close()

makemap1()
showmap()

