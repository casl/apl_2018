#include <iostream>
#include <cstdlib>
#include <math.h>
#include <vector>
#include <queue>
#include <bits/stdc++.h>

using namespace std;




struct node
{
	int x,y;
	int r, g, b;
}Node;


void swap(pair<float, pair<struct node*, struct node* > > *a, pair<float, pair<struct node*, struct node* > > *b)
{
	pair<float, pair<struct node*, struct node* > > temp;

	temp = *a;
	*a = *b;
	*b = temp;

}




void bubbleSort(vector<pair<float, pair<struct node*, struct node* > > > *edgeset, int n)
{
    int i, j;
    float s,d;
  //   cout<<"In bubble";
  //   for(int i=0; i<(*edgeset).size(); i++)
		// cout<<(*edgeset)[i].first<<"\t";
    for (i = 0; i <(*edgeset).size()-1; i++)      
	{
		
		for (j = 0; j < (*edgeset).size()-i-1; j++) 
        {
        	// cout<<"In loop "<<(*edgeset)[j].first<<" "<<(*edgeset)[j+1].first<<"\n";

        	if (((*edgeset)[j].first)>((*edgeset)[j+1].first))
        	{
        		// cout<<"before "<<(*edgeset)[j].first<<" "<<(*edgeset)[j+1].first<<"\n";
	            swap(&((*edgeset)[j]), &((*edgeset)[j+1]));
	            // cout<<"after "<<(*edgeset)[j].first<<" "<<(*edgeset)[j+1].first<<"\n";
        	}
        	
        }
	}
}

void swap1(struct node ** a, struct node ** b)
{
	struct node *temp;

	temp = *a;
	*a = *b;
	*b = temp;

}

void bubbleSortboundary(vector<struct node *> *t , int n)
{
    int i, j;
    float s,d;
  
    for (i = 0; i <n-1; i++)      
	{
		
		for (j = 0; j < n-i-1; j++) 
        {
        	// cout<<"In loop "<<(*edgeset)[j].first<<" "<<(*edgeset)[j+1].first<<"\n";

        	if ((*t)[j]->x > (*t)[j+1]->x)
        	{
        		// cout<<"before "<<(*edgeset)[j].first<<" "<<(*edgeset)[j+1].first<<"\n";
	            swap1(&((*t)[j]),&((*t)[j+1]));
	            // cout<<"after "<<(*edgeset)[j].first<<" "<<(*edgeset)[j+1].first<<"\n";
        	}
        	
        }
	}
}

void bubbleSortboundary_y(vector<struct node *> *t , int n)
{
    int i, j;
    float s,d;
  
    for (i = 0; i <n-1; i++)      
	{
		
		for (j = 0; j < n-i-1; j++) 
        {
        	// cout<<"In loop "<<(*edgeset)[j].first<<" "<<(*edgeset)[j+1].first<<"\n";

        	if ((*t)[j]->x == (*t)[j+1]->x && (*t)[j]->y > (*t)[j+1]->y)
        	{
        		// cout<<"before "<<(*edgeset)[j].first<<" "<<(*edgeset)[j+1].first<<"\n";
	            swap1(&((*t)[j]),&((*t)[j+1]));
	            // cout<<"after "<<(*edgeset)[j].first<<" "<<(*edgeset)[j+1].first<<"\n";
        	}
        	
        }
	}
}



int main()
{
	int contour;
	int size, sqrtsize;
	cin >> size >> contour;
	sqrtsize= sqrt(size);
	int count[size];
	for(int i=0; i< size; i++)
		count[i]=0;


	struct node **pixel = (struct node **)malloc(sizeof(struct node *)*size);
	struct node **pixel_list = (struct node **)malloc(sizeof(struct node *)*size);

	double egde_adj[size][8];

//==================================================Initialization of pixel=============================================

	for(int i=0; i<size; i++)
	{
		pixel[i]=(struct node *)malloc(sizeof(struct node)*8);	
		for(int j=0; j<8; j++)
		{	
			pixel[i][j].x =-1; pixel[i][j].y =-1;  pixel[i][j].r =-1; pixel[i][j].g =-1; pixel[i][j].b =-1;
		}
		
	}

//==================================================Reading File & storing in pixel ====================================

	for(int i=0; i<size; i++)
	{
		struct node *t= (struct node *)malloc(sizeof(struct node));
		
		cin >> t->x >> t->y >> t->r >> t->g >> t->b ;
		
		// cout<< t->x << " " << t->y << " " << t->r << " "  << t->g << " " << t->b <<"\n";

		pixel_list[i]=(struct node *)malloc(sizeof(struct node)*1);
		pixel_list[sqrtsize*(t->x) + (t->y)]= t;

		int cx=t->x, cy=t->y;
		// cout<< cx<<" "<<cy<<"\n";
		// cout<<"\n";
		for(int p=cx-1; p<=cx+1 ; p++)
		{
			for(int q=cy-1; q<=cy+1; q++)
			{
				if( !(p < 0 || p > sqrtsize-1 || q <0 || q > sqrtsize-1 || (p==cx && q==cy)) )
				{
					// cout<<p<<" "<< q<<"\n";
					pixel[sqrtsize*p + q][count[sqrtsize*p + q]] = *t;
					count[sqrtsize*p + q]++;
				}
			}
			
		}
		// cout<<"\n";
	}


//==================================================Initialization of edge weight==========================================
	

	for(int i=0; i<size; i++)
	{
		// cout<<"edges  \n";
		for(int j=0; j<8; j++)
		{	
			if(!(pixel[i][j].x ==-1 || pixel[i][j].y == -1))
				egde_adj[i][j] = sqrt( pow((pixel[i][j].r-pixel_list[i][0].r),2) + pow((pixel[i][j].g-pixel_list[i][0].g),2) + pow((pixel[i][j].b-pixel_list[i][0].b),2));
			else
				egde_adj[i][j] = -1;

			// cout<<pixel[i][j].x<<" " <<pixel[i][j].y<<","<< egde_adj[i][j]<<"\t";
		}
		
	}
	
	
	for(int i=0; i<size; i++)
	{
		for(int j=0; j<8; j++)
		{
			// cout<< pixel[i][j].x << "  "<<pixel[i][j].y << "  "<< pixel[i][j].r << "  "<<pixel[i][j].g << "  "<<pixel[i][j].b <<"\t\t";
		}
		// cout<<"\n";
	}

	for(int i=0; i<size; i++)
	{
		for(int j=0; j<8; j++)
		{	
			// cout<<egde_adj[i][j]<<"\t";
		}
		// cout<<"\n";
	}


//==================================================MST using Prims=============================================
	priority_queue< pair<float, pair<struct node* , struct node* > > , vector<pair<float, pair<struct node* , struct node* > > > , greater<pair<float, pair<struct node* , struct node* > > > > pq;
	
	vector<pair<float, pair<struct node*, struct node* > > > edgeset;

	bool visited[size];

	float cost[size];
	cost[0]=0;
	visited[0]=false;
	for(int i=1; i<size; i++)
	{
		visited[i]=false;
		cost[i]=INT_MAX;
	}
	
	pq.push(make_pair(0,make_pair(&pixel_list[0][0], &pixel_list[0][0])));
	
	int flag=0;
	while(!pq.empty())
	{
		pair<float, pair<struct node* , struct node* > > p = pq.top();
		pq.pop();
		// cout<<"root\n";
		// cout<<p.second.second->x<<" "<<p.second.second->y<<" "<<"\n";
		
		int index=(sqrtsize*(p.second.second->x) + (p.second.second->y));
		
		// cout<<"Index: "<<index<<","<<visited[index]<<"\n";


		if(visited[index] == true)
			continue;
		else
		{
			visited[index] = true;
			if(flag==0)
				flag=1;
			else
			{
				edgeset.push_back(make_pair(cost[index] , p.second));
				// cout<<"\n"<<p.second.first->x<<","<<p.second.first->y<<"  "<<p.second.second->x<<","<<p.second.second->y<<"  "<<p.first<<"\n";
			}
		}
		// cout<<"going inside\n";
		
		for(int i=0; i< 8; i++)
		{
			
			struct node *pa = &pixel[index][i];
			if(pa->x !=-1)
			{
				int sub_ind = (sqrtsize*pa->x + pa->y);
				if(visited[sub_ind] == false)
				{
					cost[sub_ind] = egde_adj[index][i];

					pq.push(make_pair(cost[sub_ind], make_pair(p.second.second,pa)));
					
					
				}
			}
			
		}

	}



//===============================================================================	
	// for(int i=0; i<edgeset.size(); i++)
	// 	cout<<edgeset[i].first<<"\t";

	bubbleSort(&edgeset , edgeset.size());
	// cout<<"\nSorted\n";
	// for(int i=0; i<edgeset.size(); i++)
	// 	cout<<edgeset[i].first<<"\t";
	// cout<<"\nsize  "<<edgeset.size()<<"\n";

	int delc=contour;
	while(delc)
	{
	
		edgeset.erase(edgeset.begin()+edgeset.size()-1);
		delc--;
	}
	// cout<<"\nsize  "<<edgeset.size()<<"\n";
	// for(int i=0; i<edgeset.size(); i++)
	// 	cout<<edgeset[i].first<<"\t";

// //===================================Creating new graph with mst edges====================






	struct node **newpixel = (struct node **)malloc(sizeof(struct node *)*size);

	for(int i=0; i<size; i++)
	{
		newpixel[i]=(struct node *)malloc(sizeof(struct node)*8);	
		for(int j=0; j<8; j++)
		{	
			newpixel[i][j].x =-1; newpixel[i][j].y =-1;  newpixel[i][j].r =-1; newpixel[i][j].g =-1; newpixel[i][j].b =-1;
		}
		
	}

	int newcount[size];
	for(int i=0; i<size; i++)
		newcount[i]=0;


	// cout<<"\n";
	for(int i=0; i<edgeset.size(); i++)
	{
		struct node *s=(edgeset[i].second).first;
		struct node *d=(edgeset[i].second).second;
		int sindex,dindex;

		sindex=(sqrtsize*s->x + s->y);
		dindex=(sqrtsize*d->x + d->y);
		// cout<<sindex<<" "<<dindex<<"\n";
		newpixel[sindex][newcount[sindex]]= *d;
		newpixel[dindex][newcount[dindex]]= *s;
		newcount[sindex]++;
		newcount[dindex]++;
		// cout<<"here\n";

	}
	// cout<<"new";
	for(int i=0; i<size; i++)
	{
		for(int j=0; j<8; j++)
		{	
			// cout<<newpixel[i][j].x<<" "<<newpixel[i][j].y<<"\t";
		}
		// cout<<"\n";
	}

//=====================================BFS============================================

	vector<struct node*> components;
	bool newvisited[size];
	for(int i=0; i<size; i++)
	{
		newvisited[i]=false;
	}

	queue <struct node * > q;
	
	vector< vector<struct node *> > allcontour;
	for(int i=0; i<size; i++)
	{
		

		if(newvisited[i]==false)
		{
			vector<struct node *> temp;
			q.push(pixel_list[i]);
			int findex= (sqrtsize*pixel_list[i][0].x + pixel_list[i][0].y);
			newvisited[findex]=true;
			components.push_back(pixel_list[i]);

			while(!q.empty())
			{
				struct node *n=q.front();
				q.pop();
				int nindex= (sqrtsize*n->x + n->y);
				int flag=0;
				for(int k =0; k<8; k++)
				{
					struct node *m = &newpixel[nindex][k];
					int mindex= (sqrtsize*m->x + m->y);
					if(m->x !=-1)
					{
						if(newvisited[mindex]==false)
						{
							newvisited[mindex]=true;
							q.push(m);
						}
					}
					else
					{
						if(flag==0)
						{
							temp.push_back(n);
							flag=1;
						}
					}
				}
			}
			allcontour.push_back(temp);
		}
	}

	// cout<<"\ncomponents are  "<<components.size()<<"\n";

	
	

	for(int i=0; i<allcontour.size(); i++)
	{
		// cout<<"\n size "<<allcontour[i].size()<<"\n";
		// for(int k =0; k<allcontour[i].size(); k++)
		// 	cout<<allcontour[i][k]->x<<","<<allcontour[i][k]->y<<"\t";
		// cout<<"\n";
		// bubbleSortboundary(&allcontour[i], allcontour[i].size());

		vector<struct node *> boundaryelements;
		vector<int > index_cal;

		for(int k =0; k<allcontour[i].size(); k++)
		{
			index_cal.push_back(sqrtsize*(allcontour[i][k]->x)+(allcontour[i][k]->y));
		}

		for(int k =0; k<allcontour[i].size(); k++)
		{
			vector<int > respective_ind;
			int cx=allcontour[i][k]->x, cy=allcontour[i][k]->y;
		
			for(int p=cx-1; p<=cx+1 ; p++)
			{
				for(int q=cy-1; q<=cy+1; q++)
				{
					if( !(p < 0 || p > sqrtsize-1 || q <0 || q > sqrtsize-1 || (p==cx && q==cy)) )
					{
						
						respective_ind.push_back(sqrtsize*p + q);
					}
				}
				
			}

			int nieghbours=0;
			for(int j=0; j<respective_ind.size(); j++)
			{
				for(int x =0; x<allcontour[i].size(); x++)
				{
					if(respective_ind[j]==index_cal[x])
						nieghbours++;
				}
			}
			if(nieghbours<8)
			{
				boundaryelements.push_back(allcontour[i][k]);
			}




		}
		// cout<<"Boundary :\n";
		bubbleSortboundary(&boundaryelements, boundaryelements.size());
		bubbleSortboundary_y(&boundaryelements, boundaryelements.size());


		cout<<allcontour[i].size()<<" ";
		for(int k =0; k<boundaryelements.size(); k++)
			cout<<"["<<boundaryelements[k]->x<<","<<boundaryelements[k]->y<<"] ";
		cout<<"\n";

	}



	return 1;
}


